close all;
clear all;
clc;
oldobj = instrfind; %elimina resquicios presentes na porta serial

if not(isempty(oldobj))
    fclose(oldobj);
    delete(oldobj);
end
if ~exist('s','var')
    s = serial('COM3','BaudRate',9600,'DataBits',8,'Parity','None','StopBits',1);
end   
if ~isvalid(s)
    s = serial('COM3','BaudRate',9600,'DataBits',8,'Parity','None','StopBits',l);
end     
if strcmp(get(s,'status'),'closed')
    fopen(s);
end
SENSITIVITY_ACCEL = 2.0/32768.0;   
SENSITIVITY_GYRO = 250.0/32768.0;


offset_accelx = 674.00;
offset_accely = 104.00;
offset_accelz = 16260.00;
offset_gyrox = 209.50;
offset_gyroy = -32.50;
offset_gyroz = -35.50;

disp ('En sus marcas. Posicione el sensor en la posición inicial')
pause(); %Aguarda qualquer Tecla.

disp ('comienza')

fprintf(s,'H');
i=1;

while(1) %Lee datos en un tiempo determinado en la sm32
    str{i} = fscanf(s);
    if (str{i}(1) == 'A')
        disp ('termina')
        break;
    end
    i=i+1;
end
fclose(s);
n = length(str)-1;


for i=1:n
    temp = cellfun(@str2num,strsplit(str{i},','));  %temp = eval(['[',str(i),'];']; %Selecciona un string para separarlo posteriormente
    if numel(temp) == 8
        values(i,:) = temp;
    end
end

save HOLA values

%------------------------------------------------------------------------------------------------------------------------
%                                           FIGURAS
%------------------------------------------------------------------------------------------------------------------------

Nsamples = length(values);
dt = 0.001;
t = 0:dt:Nsamples*dt-dt;

%-----------------------------------------------------------------
%                   Acelerometros RAW
%-----------------------------------------------------------------

figure;
plot(t,values(:,3)*SENSITIVITY_ACCEL,'b') %ax
hold on
plot(t,values(:,4)*SENSITIVITY_ACCEL,'r'); %ay
plot(t,values(:,5)*SENSITIVITY_ACCEL,'g'); %az
title('Acelerómetros da MPU6050 sem ca1ibracáo')
ylabel('aceleracao (g)')
xlabel('Tempo (segundos)')
legend('ax','ay','az','Location','northeast','Orientation','horizontal')
%--------------------------------------------------------------------------
%                       Acelerometros Calibrados
%--------------------------------------------------------------------------
figure;
plot(t,(values(:,3)-offset_accelx)*SENSITIVITY_ACCEL,'b') %ax
hold on
plot(t,(values(:,4)-offset_accely)*SENSITIVITY_ACCEL,'r'); %ay
plot(t,(values(:,5)-offset_accelx-(32768/2))*SENSITIVITY_ACCEL,'g'); %az
title('Acelerómetros da MPU6050 ca1ibracao')
ylabel('aceleracao (g)')
xlabel('Tempo (segundos)')
legend('ax','ay','az','Location','northeast','Orientation','horizontal')
%--------------------------------------------------------------------------
%                       Giroscopios RAW
%--------------------------------------------------------------------------
figure;
plot(t,values(:,6)*SENSITIVITY_GYRO,'b') %gx
hold on
plot(t,values(:,7)*SENSITIVITY_GYRO,'r'); %gy
plot(t,values(:,8)*SENSITIVITY_GYRO,'g'); %gz
title('Giroscopios da MPU6050 sem ca1ibracáo')
ylabel('Velocidades angular (°/s)')
xlabel('Tempo (segundos)')
legend('gx','gy','gz','Location','northeast','Orientation','horizontal')
%--------------------------------------------------------------------------
%                       Giroscopios Calibrados
%--------------------------------------------------------------------------
figure;
plot(t,(values(:,6)-offset_gyrox)*SENSITIVITY_GYRO,'b') %gx
hold on
plot(t,(values(:,7)-offset_gyroy)*SENSITIVITY_GYRO,'r'); %gy
plot(t,(values(:,8)-offset_gyroz)*SENSITIVITY_GYRO,'g'); %gz
title('Giroscopios da MPU6050 ca1ibracao')
ylabel('Velocidades angular (°/s)')
xlabel('Tempo (segundos)')
legend('gx','gy','gz','Location','northeast','Orientation','horizontal')





